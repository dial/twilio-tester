package org.digitalimpactalliance.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

@Component
@Path("/main")
public class MainService {

	Logger log = Logger.getLogger(getClass().getName());
	
	@GET
	@Path("/initial")
	public Response greetAndGather() {
		try {
			InputStream input = getClass().getClassLoader().getResourceAsStream("test.xml");
			String response = new String(IOUtils.toByteArray(input));
			log.log(Level.SEVERE, "Responding with test.xml content");
			return Response.status(200)
					.entity(response)
					.type(MediaType.APPLICATION_XML_TYPE)
					.build();
		} catch (IOException e) {
			return Response.status(500)
					.entity(e.getMessage())
					.build();
		}
	}
	
	@GET
	@Path("/action")
	public Response receiveGather(@QueryParam("Digits") String digits, 
			@QueryParam("SpeechResult") String speechResult,
			@QueryParam("Confidence") String confidence) {
		digits = StringUtils.isEmpty(digits) ? "nothing" : digits;
		speechResult = StringUtils.isEmpty(speechResult) ? "nothing" : speechResult;
		confidence = StringUtils.isEmpty(confidence) ? "nothing" : confidence;
		return Response.status(200)
				.entity("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Response><Say>" +
					    "The digits were " + digits + ", " + 
						"the speech result was " + speechResult + ", and " +
					    "the confidence was " + confidence + 
						"</Say></Response>")
				.type(MediaType.APPLICATION_XML_TYPE)
				.build();
	}

}